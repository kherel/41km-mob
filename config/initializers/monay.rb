rub = {
  :priority        => 1,
  :iso_code        => "RUB",
  :iso_numeric     => "643",
  :name            => "Russian ruble",
  :symbol          => "руб.",
  :subunit         => "коп.",
  :subunit_to_unit => 100,
  :separator       => ".",
  :delimiter       => ",",
  :html_entity     => "руб."
}
Money::Currency.register(rub)