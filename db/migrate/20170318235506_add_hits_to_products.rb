class AddHitsToProducts < ActiveRecord::Migration[5.0]
  def change
  	add_column :spree_products, :hit, :boolean, :default => false
  end
end
