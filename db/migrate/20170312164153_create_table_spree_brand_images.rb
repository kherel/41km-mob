class CreateTableSpreeBrandImages < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_brand_images do |t|
    	t.string :name
    	t.string :brand_name
    	t.boolean :active
    	t.attachment :image
    end
  end
end
