Spree::Order.class_eval do
	remove_checkout_step :delivery
	remove_checkout_step :payment

	after_validation :generate_number_soft, on: :create

	def generate_number_soft
		logger.debug(" ======================================================")
		self.number = nil
		n=1
		date = Time.current.strftime('%m-%y')
		self.number ||= loop do
	        random = "#{date}-#{n.to_s.rjust(3, '0')}"
	        if self.class.exists?(number: random)
	          n += 1 
	        else
	          break random
	        end
		end
	end
end
