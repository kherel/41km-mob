Spree::Product.class_eval do 
	belongs_to :provider

	add_search_scope :in_name_or_keywords_string do |words|
      like_any([:name, :meta_keywords], words)
    end
    
    def self.hits
    	where(:hit => true)
    end

    def self.w_btw(min, max)
    	p_props = Spree::ProductProperty.table_name
    	prop = Spree::Property.find_by_name("Ширина, см")
    	joins(:product_properties).where("#{p_props}.property_id=? and CAST(#{p_props}.value AS FLOAT) >= ? and CAST(#{p_props}.value AS FLOAT) <= ?", prop.id, min.to_f, max.to_f)
    end
    def self.h_btw(min, max)
    	p_props = Spree::ProductProperty.table_name
    	prop = Spree::Property.find_by_name("Высота, см")
    	joins(:product_properties).where("#{p_props}.property_id=? and CAST(#{p_props}.value AS FLOAT) >= ? and CAST(#{p_props}.value AS FLOAT) <= ?", prop.id, min.to_f, max.to_f)
    end
    def self.d_btw(min, max)
    		p_props = Spree::ProductProperty.table_name
    		prop = Spree::Property.find_by_name("Глубина, см")
    	joins(:product_properties).where("#{p_props}.property_id=? and CAST(#{p_props}.value AS FLOAT) >= ? and CAST(#{p_props}.value AS FLOAT) <= ?", prop.id, min.to_f, max.to_f)
    end
    def brand
    	brand = self.product_properties.find_by_property_id(Spree::Property.find_by_name("Бренд"))
    	return brand.nil? ? "" : brand.value
    end

    def country
    	country = self.product_properties.find_by_property_id(Spree::Property.find_by_name("Страна"))
    	return country.nil? ? "" : country.value
    end
end