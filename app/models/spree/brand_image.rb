module Spree
	class BrandImage < ActiveRecord::Base
		has_attached_file :image,
                      styles: { small: '150x90>' },
                      url: '/spree/brands/:id/:style/:basename.:extension',
                      path: ':rails_root/public/spree/brands/:id/:style/:basename.:extension',
                      convert_options: { all: '-strip -auto-orient -colorspace sRGB' }
	    validates_attachment :image,
	      presence: true,
	      content_type: { content_type: %w(image/jpeg image/jpg image/png image/gif) }

		def self.actived
			where(:active => true)
		end
	end
end