'use strict'
function mobileMenuOpen() {
  $(".mob-menu").addClass("mob-menu_visible")
}

function mobileMenuClose() {
  $(".mob-menu").removeClass("mob-menu_visible")
}

function mobOtherFiltersToggle(){
  const mobOtherFilters = $('.mob-filter__others')
  const btn = $('.mob-filter__btn')
  if($(mobOtherFilters[0]).hasClass('mob-filter__others_hide')){
    mobOtherFilters.removeClass("mob-filter__others_hide")
    $(btn).text("Скрыть остальные фильтры");
  } else {
    mobOtherFilters.addClass("mob-filter__others_hide")
    btn.text("Показать остальные фильтры");
  }
}

// function createCSSSelector (selector, style) {
//   if (!document.styleSheets) return;
//   if (document.getElementsByTagName('head').length == 0) return;
//
//   var styleSheet,mediaType;
//
//   if (document.styleSheets.length > 0) {
//     for (var i = 0, l = document.styleSheets.length; i < l; i++) {
//       if (document.styleSheets[i].disabled)
//         continue;
//       var media = document.styleSheets[i].media;
//       mediaType = typeof media;
//
//       if (mediaType === 'string') {
//         if (media === '' || (media.indexOf('screen') !== -1)) {
//           styleSheet = document.styleSheets[i];
//         }
//       }
//       else if (mediaType=='object') {
//         if (media.mediaText === '' || (media.mediaText.indexOf('screen') !== -1)) {
//           styleSheet = document.styleSheets[i];
//         }
//       }
//
//       if (typeof styleSheet !== 'undefined')
//         break;
//     }
//   }
//
//   if (typeof styleSheet === 'undefined') {
//     var styleSheetElement = document.createElement('style');
//     styleSheetElement.type = 'text/css';
//     document.getElementsByTagName('head')[0].appendChild(styleSheetElement);
//
//     for (i = 0; i < document.styleSheets.length; i++) {
//       if (document.styleSheets[i].disabled) {
//         continue;
//       }
//       styleSheet = document.styleSheets[i];
//     }
//
//     mediaType = typeof styleSheet.media;
//   }
//
//   if (mediaType === 'string') {
//     for (var i = 0, l = styleSheet.rules.length; i < l; i++) {
//       if(styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase()==selector.toLowerCase()) {
//         styleSheet.rules[i].style.cssText = style;
//         return;
//       }
//     }
//     styleSheet.addRule(selector,style);
//   }
//   else if (mediaType === 'object') {
//     var styleSheetLength = (styleSheet.cssRules) ? styleSheet.cssRules.length : 0;
//     for (var i = 0; i < styleSheetLength; i++) {
//       if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
//         styleSheet.cssRules[i].style.cssText = style;
//         return;
//       }
//     }
//     styleSheet.insertRule(selector + '{' + style + '}', styleSheetLength);
//   }
// }

function mobileCheck() {
  if (isMobile.any){
    $("body")
      .addClass('mobile-css')
      .css({
        'margin': '0 0 calc(2em + 2px)'
      })
    
    $('.toast-container').addClass('mob-hide')
  
    $('html').css({
      'position': 'relative',
      'min-height': '100%',
    })
    
  } else {
    $("#main-container").addClass('desktop-css')
  
    $('html').css({
      'position': 'relative'
    })
  
    $('body').css({
      'width': 'auto !important'
    })
  }
}

function mobileCheckAndRemove() {
  if (isMobile.any){

    $('.toast-container').remove()
    $('.mob-hide').remove()
    
  } else {
    $('.mob-show').remove()
  }
}
