Spree::Admin::BaseHelper.class_eval do
	def delivery_data(order)
		info = ""
		adr = order.bill_address
		info += "#{adr.first_name}, " unless adr.first_name.blank?
		info += "#{adr.address1}, " unless adr.address1.blank?
		info += "#{adr.phone}, " unless adr.phone.blank?
		info += "#{adr.comment} ," unless adr.comment.blank?
		info += "#{adr.payment_type.name}, " if adr.payment_type
		info += "#{adr.delivery_type.name}" if adr.delivery_type
		
	end

	def display_price(price)
      ActionController::Base.helpers.number_with_delimiter(price.to_i, delimiter: " ")
    end
end