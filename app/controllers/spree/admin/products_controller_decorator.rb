Spree::Admin::ProductsController.class_eval do
	def update
		if params[:product][:taxon_ids].present?
          params[:product][:taxon_ids] = params[:product][:taxon_ids].split(',')
        end
        if params[:product][:option_type_ids].present?
          params[:product][:option_type_ids] = params[:product][:option_type_ids].split(',')
        end
        if params[:product][:from_index].present?
	          	if params[:product][:available_on].present?
		        	params[:product][:available_on] = params[:product][:available_on] == "0" ?  "" : Time.current 
		        end
		        if @object.sku == "" || @object.sku.nil? 
		        	last_sku = Spree::Variant.order("sku").last.sku
		        	curr_sku = last_sku == ""||last_sku.nil? ? "1".rjust(5, '0') : (last_sku.to_i+1).to_s.rjust(5, '0')
		        	params[:product][:sku] = curr_sku
		        end 
		end
        invoke_callbacks(:update, :before)
        if @object.update_attributes(permitted_resource_params)
          invoke_callbacks(:update, :after)
          flash[:success] = flash_message_for(@object, :successfully_updated)
          	if params[:product][:from_index].present?
		        @collection = collection
	          	respond_with(@collection) do |format|
		            format.html { redirect_to  :back }
		            format.js   { render layout: false }
		        end
          	else
		        respond_with(@object) do |format|
		            format.html { redirect_to location_after_save }
		            format.js   { render layout: false }
		        end
	      	end
        else
          	@product.slug = @product.slug_was if @product.slug.blank?
          	invoke_callbacks(:update, :fails)
          	respond_with(@object)
        end
	end
	def index
		session[:return_to] = request.url
		@collection = @collection.joins(:master).order("spree_variants.sku asc")  if params[:q][:s].present? && params[:q][:s] == "sku asc"
		@collection = @collection.joins(:master).order("spree_variants.sku desc")  if params[:q][:s].present? && params[:q][:s] == "sku desc" 
		respond_with(@collection)
	end
	# def collection
 #        return @collection if @collection.present?
 #        params[:q] ||= {}
 #        params[:q][:deleted_at_null] ||= "1"
 #        params[:q][:not_discontinued] ||= "1"

 #        params[:q][:s] ||= "name asc"
 #        if params[:q][:s] == 'sku asc'
 #        	params[:q][:s] == 'spree_variants.sku asc'
 #        end
 #        if params[:q][:s] == 'sku desc'
 #        	params[:q][:s] == 'spree_variants.sku desc'
 #        end
 #        @collection = super
 #        # Don't delete params[:q][:deleted_at_null] here because it is used in view to check the
 #        # checkbox for 'q[deleted_at_null]'. This also messed with pagination when deleted_at_null is checked.
 #        if params[:q][:deleted_at_null] == '0'
 #          @collection = @collection.with_deleted
 #        end
 #        # @search needs to be defined as this is passed to search_form_for
 #        # Temporarily remove params[:q][:deleted_at_null] from params[:q] to ransack products.
 #        # This is to include all products and not just deleted products.
 #        @search = @collection.ransack(params[:q].reject { |k, _v| k.to_s == 'deleted_at_null' })
 #        @collection = @search.result.
 #              distinct_by_product_ids(params[:q][:s]).
 #              includes(product_includes).
 #              page(params[:page]).
 #              per(params[:per_page] || Spree::Config[:admin_products_per_page])
 #        @collection
 #      end
end