Spree::Admin::OrdersController.class_eval do 
	def index
		params[:q] ||= {}
        params[:q][:completed_at_not_null] ||= '1' if Spree::Config[:show_only_complete_orders_by_default]
        @show_only_completed = params[:q][:completed_at_not_null] == '1'
        params[:q][:s] ||= @show_only_completed ? 'completed_at desc' : 'created_at desc'
        params[:q][:completed_at_not_null] = '' unless @show_only_completed
        created_at_gt = params[:q][:created_at_gt]
        created_at_lt = params[:q][:created_at_lt]
        params[:q].delete(:inventory_units_shipment_id_null) if params[:q][:inventory_units_shipment_id_null] == "0"
        if params[:q][:created_at_gt].present?
          params[:q][:created_at_gt] = Time.zone.parse(params[:q][:created_at_gt]).beginning_of_day rescue ""
        end
        if params[:q][:created_at_lt].present?
          params[:q][:created_at_lt] = Time.zone.parse(params[:q][:created_at_lt]).end_of_day rescue ""
        end
        if @show_only_completed
          params[:q][:completed_at_gt] = params[:q].delete(:created_at_gt)
          params[:q][:completed_at_lt] = params[:q].delete(:created_at_lt)
        end

        @search = Spree::Order.preload(:user).includes(:bill_address).accessible_by(current_ability, :index).ransack(params[:q])
        @orders = @search.result(distinct: true).
          page(params[:page]).
          per(params[:per_page] || Spree::Config[:admin_orders_per_page])
        @orders = @orders.order("spree_addresses.firstname ASC")  if params[:q][:s] == "customer asc"
        @orders = @orders.order("spree_addresses.firstname DESC")  if params[:q][:s] == "customer desc" 
        @orders = @orders.order("spree_addresses.phone ASC")  if params[:q][:s] == "phone asc"
        @orders = @orders.order("spree_addresses.phone DESC")  if params[:q][:s] == "phone desc" 
        # Restore dates
        params[:q][:created_at_gt] = created_at_gt
        params[:q][:created_at_lt] = created_at_lt
	end
end