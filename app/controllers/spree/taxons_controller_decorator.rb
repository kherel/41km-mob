Spree::TaxonsController.class_eval do 

	  helper_method :sorting_param
    alias_method :old_show, :show

    def show
      old_show
      # @taxon = Spree::Taxon.friendly.find(params[:id])
      # return unless @taxon
      # @searcher = build_searcher(params.merge(taxon: @taxon.id, include_images: true))
      # # @products = @products.reorder('').send(sorting_scope) if params[:sorting].present?
      # @taxonomies = Spree::Taxonomy.includes(root: :children)
      @a_prods = Spree::Product.in_taxon(@taxon).available
      if @a_prods.any?
        @min_product_price = @a_prods.select('spree_products.*, spree_prices.amount').reorder('').send(:ascend_by_master_price).first.price.to_i
        @max_product_price = @a_prods.select('spree_products.*, spree_prices.amount').reorder('').send(:ascend_by_master_price).last.price.to_i
        
      end
      @min_product_price ||= 0
      @max_product_price ||= 0
      @products = @products.h_btw(params[:h_btw_min], params[:h_btw_max]) if params[:h_btw_min].present?&&params[:h_btw_max].present?
      @products = @products.d_btw(params[:d_btw_min], params[:d_btw_max]) if params[:d_btw_min].present?&&params[:d_btw_max].present?
      @products = @products.w_btw(params[:w_btw_min], params[:w_btw_max]) if params[:w_btw_min].present?&&params[:w_btw_max].present?
      @products = @products.select('spree_products.*, spree_prices.amount').reorder('').send(:ascend_by_master_price)
      @products = @products.master_price_lte(params[:max_price].to_f) if params[:max_price]&&!params[:max_price].blank?
      @products = @products.master_price_gte(params[:min_price].to_f) if params[:min_price]&&!params[:min_price].blank? #if params.key?(:minprice) @&& params.key?(:maxprice)
      
    end

    def sorting_param
      params[:sorting].try(:to_sym) || default_sorting
    end

    
    private

    def sorting_scope
      allowed_sortings.include?(sorting_param) ? sorting_param : default_sorting
    end

    def default_sorting
      :ascend_by_master_price
    end

    def allowed_sortings
      [:descend_by_master_price, :ascend_by_master_price, :ascend_by_updated_at]
    end

    # def base_scope_c(taxon)
    #   base_scope = Spree::Product.spree_base_scopes.active
    #   base_scope = base_scope.in_taxon(taxon) unless taxon.blank?
    #   base_scope = get_products_conditions_for(base_scope, keywords)
    #   base_scope = add_search_scopes(base_scope)
    #   base_scope = add_eagerload_scopes(base_scope)
    #   base_scope
    # end
            

end